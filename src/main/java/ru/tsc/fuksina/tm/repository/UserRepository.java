package ru.tsc.fuksina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.IUserRepository;
import ru.tsc.fuksina.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
