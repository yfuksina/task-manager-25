package ru.tsc.fuksina.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
        super();
    }

    public AbstractFieldException(@NotNull final  String message) {
        super(message);
    }

    public AbstractFieldException(@NotNull final  String message, @NotNull final  Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(@NotNull final  Throwable cause) {
        super(cause);
    }

    protected AbstractFieldException(
            @NotNull final  String message,
            @NotNull final  Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
